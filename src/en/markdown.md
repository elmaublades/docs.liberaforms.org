# Markdown

[https://commonmark.org](https://commonmark.org)

## Titles

Titles are lines of text that begin with a `#` followed by a space.


**This markdown syntax ..**
```
# A big title
```

**.. produces**
# A big title


Smaller titles begin with two `##`

**This markdown syntax ..**
```
## A smaller title
```

**.. produces**
## A smaller title



## Lists

Begin each line with a `*` followed by a space.

**This markdown syntax ..**
```
* The first item
* Number two
* The last item
```

**.. produces**
* The first item
* Number two
* The last item

### Numbered lists

Begin each line with `1.` followed by a space.

**This markdown syntax ..**
```
1. The first item
2. Number two
3. The last item
```

**.. produces**
1. The first item
2. Number two
3. The last item

### Indented lists

Create an indented list by adding **two** empty spaces to the beginning of the line.

**This markdown syntax ..**
```
* The first item
* Number two
  1. A sub item
  2. And another
* The last item
  * More last item items
  * The very last item
```

**.. produces**
* The first item
* Number two
  1. A sub item
  2. And another
* The last item
  * More last item items
  * The very last item


## Links


## Images




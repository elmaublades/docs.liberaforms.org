# Profile

This page lets you do some basic stuff like:

* Change your email address
* Change your password
* Change the LiberaForms' language

## By default

### New answer notification

When someone answers one of your forms you will be notified by email.

Use this option to set that notification for all the new forms your create.

## Delete my account

When you delete your account, all the forms you have created and their answers will also be deleted.

Before deleting your account you will be prompted to enter your username, just to be sure.

---

## My admin settings

### Notify me when new user has registered
You will be notified by email when a new user has registered

### Notify me when new form has been created
You will be notified by email when a new form has been created




# Create a form

Create a form with the `New form` button.

![New form button](/images/new_form_button.en.png)

The New form page has three parts.

1. Introduction text
2. The form
3. The form URL

## 1. Introduction text
Your new form will begin with this text. This is the place to explain what your form is about and why people should fill it out. You can include as much detail as you wish.

The editor may seem a little strange at first because of different symbols, like `#` that get added to your text. This is `Markdown` syntax. You can **safetly ignore** this syntax if you wish, however markdown is widely used in LiberaForms because it is an easy way to write HTML code.

> Markdown is easy. [Learn how in 2 minutes](/en/markdown.html).


## 2. The form

A form contains `elements`. Elements are the building blocks of a form.

There are different types of elements. One for short texts, another for long texts, checkboxes, dates, etc.

You build your form by dragging `elements` from the right into the empty box.

To ask for a person's name for example, drag a `Text Field` element into the box. A Text Field is a **one line** text element.
It is ideal for short texts like a Name.
  
> Please see the complete list of [form elements and their options](/en/forms/elements.html)

 
### Managing elements

Each element is displayed by a block that contains the `Label` on the left, and `Delete`, `Edit`, and `Duplicate` buttons on the right.

![Element block](/images/element_block.en.png)

You can change the order of the elements within your form by dragging and dropping the blocks.

#### Deleting elements

You can delete an element. 


## 3. The form URL
Your form will have this address. People will use to find your form on the Internet.

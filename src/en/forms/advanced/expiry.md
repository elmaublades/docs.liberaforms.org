# Expiry conditions

You can set some conditions that, when met, the form will not be available.

## Date / Time

The form will not be available after this date.

## Maximum answers

The form will not be available after this number of answers have been submitted.

## Other conditions

If you include a `Number` field in your form, an option to define a maximum will be displayed.
LiberaForms will add all the answers together. The form will not be available when the maximum is met.

Learn more about [`Number` field options](/en/forms/elements.html#number).

## When the form has expired

This text will be displayed when the form has expired.
